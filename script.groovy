def pakcage(){
    sh "mvn package -DskipTests"

}

def buildDockerImage(){
    withCredentials([usernamePassword(credentialsId:'dockerhub',usernameVariable:"USER",passwordVariable:"PASSWD")]){
        sh "docker build -t hamzaezh/jenkinsdemo:pipeline ."
        sh "echo $PASSWD | docker login -u $USER --password-stdin"
        sh "docker push hamzaezh/jenkinsdemo:pipeline"
        sh "docker rmi hamzaezh/jenkinsdemo:pipeline"

    }

}
return this 
