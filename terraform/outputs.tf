output "dev_server_public_ip" {
  value = module.dev-server.instance.public_ip
}
output "dev_server_public_dns" {
  value = module.dev-server.instance.public_dns
}