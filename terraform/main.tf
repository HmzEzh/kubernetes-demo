#create vpc
resource "aws_vpc" "dev_vpc" {
  cidr_block = var.vpc_cidr_block
  enable_dns_hostnames = true
  tags = {
    Name: "${var.env_prefix} vpc"
  }
  
}
module "dev-subnet" {
  source = "./modules/subnet"
  dev_vpc_id = aws_vpc.dev_vpc.id
  availability_zone = var.availability_zone
  subnet_cidr_block = var.subnet_cidr_block
  env_prefix = var.env_prefix
  
}
module "dev-server" {
  source = "./modules/webserver"
  dev_vpc_id = aws_vpc.dev_vpc.id
  dev_subnet_id = module.dev-subnet.dev_subnet.id
  availability_zone = var.availability_zone
  env_prefix = var.env_prefix
  ami = var.ami
  public_key = var.public_key
  private_key = var.private_key
  instance_type = var.instance_type
  
}

