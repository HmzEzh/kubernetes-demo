#create security groupes
resource "aws_security_group" "security_group" {
  vpc_id = var.dev_vpc_id
  ingress  {
    cidr_blocks = ["0.0.0.0/0"]
    from_port = 80
    to_port = 80
    protocol = "tcp"
  }
  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port = 22
    to_port = 22
    protocol = "tcp"
  }
  egress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port = 0
    to_port = 0
    protocol = "-1"
  }
  tags = {
    Name: "${var.env_prefix} security group "
  }
  
}

#query the ami
#craete ssh key-paire
resource "aws_key_pair" "dev_server_key" {
  key_name = "dev-server-key"
  public_key = file(var.public_key)
  
}
#create ec2 
resource "aws_instance" "dev_serer" {
  ami = var.ami
  instance_type = var.instance_type
  vpc_security_group_ids = [aws_security_group.security_group.id]
  availability_zone = var.availability_zone
  subnet_id = var.dev_subnet_id
  associate_public_ip_address = true
  user_data = file("./user-data.sh")
  key_name = aws_key_pair.dev_server_key.key_name
  tags = {
    Name: "${var.env_prefix} server"
  }
  provisioner "remote-exec" {
    connection {
      type = "ssh"
      host = self.public_ip
      user = "ubuntu"
      private_key = file(var.private_key)
    }
    inline = [ "mkdir hamza" ]
    
  }
  
}
