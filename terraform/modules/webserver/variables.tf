variable "dev_vpc_id" {}
variable "dev_subnet_id" {}
variable "env_prefix" {}
variable "availability_zone" {}
variable "instance_type" {}
variable "ami" {}
variable "public_key" {}
variable "private_key" {}