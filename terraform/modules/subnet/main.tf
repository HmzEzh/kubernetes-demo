
#create subnet 
resource "aws_subnet" "dev_subnet" {
  vpc_id = var.dev_vpc_id
  cidr_block = var.subnet_cidr_block
  availability_zone = var.availability_zone
  map_public_ip_on_launch = true
  tags = {
    Name: "${var.env_prefix} subnet"
  }
  
}

#create internet gateway
resource "aws_internet_gateway" "internet-gateway" {
  vpc_id = var.dev_vpc_id
  tags = {
    Name: "${var.env_prefix} internet gateway"
  }
  
}
#craete a route table
resource "aws_route_table" "route_table" {
  vpc_id = var.dev_vpc_id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.internet-gateway.id
  }
  tags = {
    Name: "${var.env_prefix} route table "
  }
  
}
resource "aws_route_table_association" "route_table_association" {
  route_table_id = aws_route_table.route_table.id
  subnet_id = aws_subnet.dev_subnet.id
}