variable "vpc_cidr_block" {}
variable "subnet_cidr_block" {}
variable "env_prefix" {}
variable "availability_zone" {}
variable "instance_type" {}
variable "ami" {}
variable "public_key" {}
variable "private_key" {}