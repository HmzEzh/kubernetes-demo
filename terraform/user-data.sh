#!/bin/bash

sudo apt update
sudo apt install -y docker.io
sudo docker run --name nginx-server -p 80:80 -d nginx:latest
