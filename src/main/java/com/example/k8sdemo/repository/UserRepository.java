package com.example.k8sdemo.repository;

import com.example.k8sdemo.beans.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Integer> {



}
