FROM eclipse-temurin:19
RUN mkdir /app
COPY /target/k8s-demo-*.jar /app/app.jar
ENTRYPOINT ["java", "-jar", "/app/app.jar"]